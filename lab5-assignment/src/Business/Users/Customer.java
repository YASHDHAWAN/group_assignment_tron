/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;

/**
 *
 * @author Hardik Bhuvil Soni
 */
public class Customer extends User implements Comparable<Customer>{

    public Customer(String emailAddress, String password) {
        super(emailAddress, password, "CUSTOMER");
    }
    
    @Override
    public int compareTo(Customer o) {
        return o.getEmailAddress().compareTo(this.getEmailAddress());
    }

    @Override
    public String toString() {
        return getEmailAddress(); //To change body of generated methods, choose Tools | Templates.
    }
    public boolean verifyPass(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }
    
}
